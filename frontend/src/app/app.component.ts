import { Component, ViewChild } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {BehaviorSubject, Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";

interface Valute {
  id: number;
  ticker: string;
  rate: number;
}

interface Transaction {
  id: number;
  firstValuteTicker: string;
  secondValuteTicker: string;
  firstValuteRateToRub: number;
  secondValuteRateToRub: number;
  firstValuteQuantity: number;
  secondValuteQuantity: number;
  date: Date;
}

interface Statistic {

}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  BACKEND_LINK = 'http://localhost:8080/'
  transactions!: Transaction[];
  convertForm!: FormGroup;
  updateData$: Subject<boolean> = new BehaviorSubject<boolean>(false);
  valutes: Valute[] = [];
  currentDate = new Date();
  convertResult: number = 0;
  convertResultTicker: string = "";
  statistics1!: Statistic[];
  statistics2!: Statistic[];

  // Данные для таблицы с историей конвертаций
  displayedColumns: string[] = ['id', 'firstValuteTicker', 'firstValuteRateToRub', 'firstValuteQuantity',
    'secondValuteTicker', 'secondValuteRateToRub', 'secondValuteQuantity', 'date'];
  dataSource = new MatTableDataSource(this.transactions);

  // Данные для таблицы со статистикой по переводимым валютам за неделю
  displayedColumnsStatistics1: string[] = ['ticker', 'avgRate', 'sumQuantity'];
  dataSourceStatistics1 = new MatTableDataSource(this.statistics1);

  // Данные для таблицы со статистикой по получаемым валютам за неделю
  displayedColumnsStatistics2: string[] = ['ticker', 'avgRate', 'sumQuantity'];
  dataSourceStatistics2 = new MatTableDataSource(this.statistics2);

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private http: HttpClient) {
  }

  // Фильтр
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit(): void {
    this.convertForm = new FormGroup({
      firstTicker: new FormControl(''),
      secondTicker: new FormControl(''),
      quantity: new FormControl('', Validators.pattern(/^-?(0|[1-9]\d*)?$/))
    })

    this.getAllValutes();
    this.getAllTransactions();
    this.getStatistics1();
    this.getStatistics2();
  }

  // Отправка формы
  submit() {
    this.updateData$.next(true);
    this.http.post<Transaction>(this.BACKEND_LINK + 'transaction', {
      "id": null,
      "firstValuteTicker": this.convertForm.value.firstTicker.split(" ")[0],
      "secondValuteTicker": this.convertForm.value.secondTicker.split(" ")[0],
      "firstValuteRateToRub": this.convertForm.value.firstTicker.split(" ")[1],
      "secondValuteRateToRub": this.convertForm.value.secondTicker.split(" ")[1],
      "firstValuteQuantity": this.convertForm.value.quantity,
      "secondValuteQuantity": this.convertForm.value.quantity * this.convertForm.value.firstTicker.split(" ")[1] / this.convertForm.value.secondTicker.split(" ")[1],
      "date": Date.now()
    }).subscribe(response => {
      this.updateData$.next(false);
      this.convertResult = response.secondValuteQuantity;
      this.convertResultTicker = response.secondValuteTicker;
      this.getAllTransactions();
      this.getStatistics1();
      this.getStatistics2();
    })
  }

  // Получить все валюты
  getAllValutes() {
    this.http.get<Valute[]>(this.BACKEND_LINK + 'valute')
      .subscribe(response => {
        this.valutes = response;
      })
  }

  // Получить все транзакции
  getAllTransactions() {
    this.http.get<Transaction[]>(this.BACKEND_LINK + 'transaction')
      .subscribe(response => {
        this.transactions = response;
        this.dataSource = new MatTableDataSource(this.transactions);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      })
  }

  // Получить статистику за неделю для переводимых валют
  getStatistics1() {
    this.http.get<Object[]>(this.BACKEND_LINK + 'statistics1')
      .subscribe(response => {
        this.statistics1 = response;
        this.dataSourceStatistics1 = new MatTableDataSource(this.statistics1);
        // this.dataSource.sort = this.sort;
        // this.dataSource.paginator = this.paginator;
      })
  }

  // Получить статистику за неделю для получаемых валют
  getStatistics2() {
    this.http.get<Object[]>(this.BACKEND_LINK + 'statistics2')
      .subscribe(response => {
        this.statistics2 = response;
        this.dataSourceStatistics2 = new MatTableDataSource(this.statistics2);
        // this.dataSource.sort = this.sort;
        // this.dataSource.paginator = this.paginator;
      })
  }
}

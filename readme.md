Проект выполнялся в соответствии с техническим заданием:

https://sites.google.com/iteratia.com/java-test-cc

Проект настроен на работу на локальных портах:

Фронтенд (Angular): http://localhost:4200/
Бэкенд (Java + Spring): http://localhost:8080/

Для проверки, фронтенд и бэкенд можно запустить отдельно в соответствующих средах разработки (например, WebStorm для фронтенда и IDEA для бэкенда).

Для подключения к базе данных PostgreSQL используются следующие данные (база с этими данными должна быть предварительно создана):

Название базы: parser
Имя пользователя: postgres
Пароль: springcourse

Данные настройки указаны в файле backend/src/main/resources/application.properties.

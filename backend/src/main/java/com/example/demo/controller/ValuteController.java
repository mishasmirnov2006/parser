package com.example.demo.controller;

import com.example.demo.entities.Transaction;
import com.example.demo.entities.Valute;
import com.example.demo.service.TransactionService;
import com.example.demo.service.ValuteService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping
@RequiredArgsConstructor
@Transactional
public class ValuteController {
    @Autowired
    private ValuteService valuteService;

    // Получить список валют и их курсов
    @GetMapping("/valute")
    public List<Valute> getAllValutes() throws Exception {
        return valuteService.getRates();
    }
}

package com.example.demo.controller;

import com.example.demo.entities.Transaction;
import com.example.demo.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping
@RequiredArgsConstructor
@Transactional
public class TransactionController {
    @Autowired
    private TransactionService transactionService;

    // Получить список всех транзакций
    @GetMapping("/transaction")
    public List<Transaction> getAllTransactions() {
        return transactionService.getAllTransactions();
    }

    // Сохранение транзации
    @PostMapping("/transaction")
    public Transaction saveTransaction(@RequestBody Transaction transaction) {
        return transactionService.saveTransaction(transaction);
    }

    // Получить статистику за неделю для переводимых валют
    @GetMapping("/statistics1")
    public List getStatistics1() {
        return transactionService.getStatistics1();
    }

    // Получить статистику за неделю для получаемых валют
    @GetMapping("/statistics2")
    public List getStatistics2() {
        return transactionService.getStatistics2();
    }
}

package com.example.demo.service;

import com.example.demo.entities.Transaction;
import com.example.demo.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    // Сохранение транзации
    public Transaction saveTransaction(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    // Получение всех транзакций
    public List<Transaction> getAllTransactions() {
        return transactionRepository.findAll();
    }

    // Получить статистику за неделю для переводимых валют
    public List getStatistics1() {
        return transactionRepository.findStatistic1();
    }

    // Получить статистику за неделю для получаемых валют
    public List getStatistics2() {
        return transactionRepository.findStatistic2();
    }
}

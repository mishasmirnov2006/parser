package com.example.demo.service;

import com.example.demo.entities.Transaction;
import com.example.demo.entities.Valute;
import com.example.demo.repositories.ValuteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ValuteService {

    @Autowired
    private ValuteRepository valuteRepository;

    // Получение коллекции с объектами валют (с тикерами и курсами) с сайта CBR и их сохранение (обновление) в базе данных
    public List<Valute> getRates() throws Exception {
        HashMap<String, NodeList> result = new HashMap(); // служебная коллекция для дальнейших операций
        String[][] rates = null; // массив для хранения курсов валют в формате "Тикер - Курс единицы валюты в рублях"
        List<Valute> valutes = new ArrayList<Valute>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); // класс для форматирования даты
        Date date = new Date(); // получаем текущую дату
        String url = "http://www.cbr.ru/scripts/XML_daily.asp?date_req=" + dateFormat.format(date); // ссылка с текущей отформатированной датой
        Document doc = loadDocument(url); // загружаем документ по ссылке с помощью специального класса

        NodeList nl = doc.getElementsByTagName("Valute"); // получаем из XML-файла все ноды с именем тега "Valute"

        // Перебираем полученные ноды через цикл и добавляем в коллекцию, где ключ - тикер валюты, значение - все остальные данные из ноды, которые потом будем обрабатывать отдельно
        for (int i = 0; i < nl.getLength(); i++) {
            Node c = nl.item(i);
            NodeList nlChilds = c.getChildNodes();
            for (int j = 0; j < nlChilds.getLength(); j++) {
                if (nlChilds.item(j).getNodeName().equals("CharCode")) {
                    result.put(nlChilds.item(j).getTextContent(), nlChilds);
                }
            }
        }

        int k = 0;
        rates = new String[result.size()][2];

        // Перебираем коллекцию, чтобы вычислить курс каждой валюты (стоимость единицы в рублях)
        for (Map.Entry<String, NodeList> entry : result.entrySet()) {
            NodeList temp = entry.getValue();
            double value = 0;
            int nominal = 0;
            for (int i = 0; i < temp.getLength(); i++) {
                if (temp.item(i).getNodeName().equals("Value")) {
                    value = Double.parseDouble(temp.item(i).getTextContent().replace(',', '.')); // получаем курс лота в рублях
                }
                else if (temp.item(i).getNodeName().equals("Nominal")) {
                    nominal = Integer.parseInt(temp.item(i).getTextContent()); // получаем размер лота
                }
            }
            double amount = value / nominal; // получаем стоимость единицы валюты в рублях
            rates[k][0] = entry.getKey();
            rates[k][1] = (((double) Math.round(amount * 10000)) / 10000) + " рублей"; // способ округления до 4 знаков после запятой после деления
            k++;
            Valute valute = new Valute();
            valute.setTicker(entry.getKey());
            valute.setRate(((double) Math.round(amount * 10000)) / 10000);
            valutes.add(valute);
        }

        // Сохраняем (обновляем, добавляем) данные в базе данных
        for (Valute valute : valutes) {
            if(getValuteByTicker(valute.getTicker()) != null) {
                getValuteByTicker(valute.getTicker()).setRate(valute.getRate());
            } else {
                valuteRepository.save(valute);
            }
        }

        return valuteRepository.findAll();
    }

    // Метод для парсинга XML-файла
    private Document loadDocument(String url) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        return factory.newDocumentBuilder().parse(new URL(url).openStream());
    }

    // Получение валюты по тикеру
    public Valute getValuteByTicker(String ticker) {
        Optional<Valute> valute = valuteRepository.findByTicker(ticker);
        if (valute.isPresent()) {
            return valute.get();
        } else {
            return null;
        }
    }
}

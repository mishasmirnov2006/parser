package com.example.demo.repositories;

import com.example.demo.entities.Transaction;
import com.example.demo.entities.Valute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ValuteRepository extends JpaRepository<Valute, Long> {

    Optional<Valute> findById(Long id);
    Optional<Valute> findByTicker(String ticker);

}

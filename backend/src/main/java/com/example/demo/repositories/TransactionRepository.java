package com.example.demo.repositories;

import com.example.demo.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    Optional<Transaction> findById(Long id);

    // Получить статистику за неделю для переводимых валют
    @Query(value = "SELECT first_valute_ticker, avg(first_valute_rate_to_rub), SUM(first_valute_quantity)\n" +
            "FROM public.transactions\n" +
            "where date > (NOW() - interval'7 day')\n" +
            "group by first_valute_ticker", nativeQuery = true)
    List findStatistic1();

    // Получить статистику за неделю для получаемых валют
    @Query(value = "SELECT second_valute_ticker, avg(second_valute_rate_to_rub), SUM(second_valute_quantity)\n" +
            "FROM public.transactions\n" +
            "where date > (NOW() - interval'7 day')\n" +
            "group by second_valute_ticker", nativeQuery = true)
    List findStatistic2();

}

package com.example.demo.entities;

import lombok.*;
import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "transactions")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_valute_ticker")
    private String firstValuteTicker;

    @Column(name = "second_valute_ticker")
    private String secondValuteTicker;

    @Column(name = "first_valute_rate_to_rub")
    private double firstValuteRateToRub;

    @Column(name = "second_valute_rate_to_rub")
    private double secondValuteRateToRub;

    @Column(name = "first_valute_quantity")
    private double firstValuteQuantity;

    @Column(name = "second_valute_quantity")
    private double secondValuteQuantity;

    @Column(name = "date")
    private Date date;
}
